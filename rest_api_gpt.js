const axios = require('axios');

const prompt = "Translate the following English text to French: '{text: 'Hello, world!'}'";

axios.post('https://api.openai.com/v1/engines/davinci-codex/completions', {
    prompt: prompt,
    max_tokens: 60
}, {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer YOUR_OPENAI_API_KEY'
    }
})
.then(response => {
    console.log(response.data.choices[0].text.trim());
})
.catch(error => {
    console.error(error);
});
