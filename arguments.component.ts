import { Component } from '@angular/core';
import { ArgumentService } from '../services/argument.service';

interface Arguments {
    originalStatement: string;
    supportingArguments: string[];
    opposingArguments: string[];
}

@Component({
    selector: 'app-arguments',
    templateUrl: './arguments.component.html',
    styleUrls: ['./arguments.component.scss']
})
export class ArgumentsComponent {

    statement: string;
    arguments: Arguments;

    constructor(private argumentService: ArgumentService) { }

    getArguments(statement: string): void {
        this.argumentService.getArguments(statement)
      .subscribe((arguments: Arguments) => this.arguments = { ...arguments });
    }
}
